﻿namespace MySerial
{
    partial class Serial_Con
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sp_Main = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbx_Serial = new DMSkin.Metro.Controls.MetroComboBox();
            this.cbx_BaudRate = new DMSkin.Metro.Controls.MetroComboBox();
            this.pic_Stat = new System.Windows.Forms.PictureBox();
            this.btn_Con = new DMSkin.Controls.DMButton();
            this.metroToolTip1 = new DMSkin.Metro.Components.MetroToolTip();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Stat)).BeginInit();
            this.SuspendLayout();
            // 
            // sp_Main
            // 
            this.sp_Main.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.sp_Main_DataReceived);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(3, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "波特率：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "串口号：";
            // 
            // cbx_Serial
            // 
            this.cbx_Serial.DM_FontSize = DMSkin.Metro.MetroComboBoxSize.Small;
            this.cbx_Serial.DM_UseSelectable = true;
            this.cbx_Serial.DropDownHeight = 105;
            this.cbx_Serial.DropDownWidth = 260;
            this.cbx_Serial.FormattingEnabled = true;
            this.cbx_Serial.IntegralHeight = false;
            this.cbx_Serial.ItemHeight = 21;
            this.cbx_Serial.Location = new System.Drawing.Point(72, 5);
            this.cbx_Serial.Name = "cbx_Serial";
            this.cbx_Serial.Size = new System.Drawing.Size(129, 27);
            this.cbx_Serial.TabIndex = 1;
            this.cbx_Serial.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            this.cbx_Serial.SelectedIndexChanged += new System.EventHandler(this.cbx_Serial_SelectedIndexChanged);
            // 
            // cbx_BaudRate
            // 
            this.cbx_BaudRate.DM_FontSize = DMSkin.Metro.MetroComboBoxSize.Small;
            this.cbx_BaudRate.DM_UseSelectable = true;
            this.cbx_BaudRate.FormattingEnabled = true;
            this.cbx_BaudRate.ItemHeight = 21;
            this.cbx_BaudRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "28800",
            "38400",
            "57600",
            "115200"});
            this.cbx_BaudRate.Location = new System.Drawing.Point(72, 40);
            this.cbx_BaudRate.Name = "cbx_BaudRate";
            this.cbx_BaudRate.Size = new System.Drawing.Size(92, 27);
            this.cbx_BaudRate.TabIndex = 2;
            this.cbx_BaudRate.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            // 
            // pic_Stat
            // 
            this.pic_Stat.Image = global::MySerial.Properties.Resources.lost;
            this.pic_Stat.Location = new System.Drawing.Point(207, 5);
            this.pic_Stat.Name = "pic_Stat";
            this.pic_Stat.Size = new System.Drawing.Size(27, 27);
            this.pic_Stat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Stat.TabIndex = 2;
            this.pic_Stat.TabStop = false;
            this.metroToolTip1.SetToolTip(this.pic_Stat, "点击刷新");
            this.pic_Stat.Click += new System.EventHandler(this.pic_Stat_Click);
            // 
            // btn_Con
            // 
            this.btn_Con.BackColor = System.Drawing.Color.Transparent;
            this.btn_Con.DM_DisabledColor = System.Drawing.Color.Empty;
            this.btn_Con.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.btn_Con.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btn_Con.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Con.DM_Radius = 1;
            this.btn_Con.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Con.ForeColor = System.Drawing.Color.White;
            this.btn_Con.Image = null;
            this.btn_Con.Location = new System.Drawing.Point(172, 38);
            this.btn_Con.Name = "btn_Con";
            this.btn_Con.Size = new System.Drawing.Size(64, 31);
            this.btn_Con.TabIndex = 3;
            this.btn_Con.Text = "连接";
            this.btn_Con.UseVisualStyleBackColor = false;
            this.btn_Con.Click += new System.EventHandler(this.btn_Con_Click);
            // 
            // metroToolTip1
            // 
            this.metroToolTip1.AutoPopDelay = 3000;
            this.metroToolTip1.InitialDelay = 300;
            this.metroToolTip1.ReshowDelay = 100;
            this.metroToolTip1.Style = DMSkin.Metro.MetroColorStyle.Blue;
            this.metroToolTip1.StyleManager = null;
            this.metroToolTip1.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            // 
            // Serial_Con
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cbx_BaudRate);
            this.Controls.Add(this.cbx_Serial);
            this.Controls.Add(this.btn_Con);
            this.Controls.Add(this.pic_Stat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(240, 72);
            this.MinimumSize = new System.Drawing.Size(240, 72);
            this.Name = "Serial_Con";
            this.Size = new System.Drawing.Size(240, 72);
            this.Load += new System.EventHandler(this.Serial_Con_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Stat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort sp_Main;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DMSkin.Metro.Controls.MetroComboBox cbx_Serial;
        private DMSkin.Metro.Controls.MetroComboBox cbx_BaudRate;
        private System.Windows.Forms.PictureBox pic_Stat;
        private DMSkin.Controls.DMButton btn_Con;
        private DMSkin.Metro.Components.MetroToolTip metroToolTip1;
    }
}
