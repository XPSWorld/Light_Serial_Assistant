﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySerial.Class_Serial
{
    public class Serial_Info
    {
        
        /// <summary>
        /// 串口号
        /// </summary>
        public string Name { get;  set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Des { get;  set; }

        /// <summary>
        /// 获取所有串口号
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name + " - " + Des;
        }
    }
}
