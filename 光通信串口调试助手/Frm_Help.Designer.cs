﻿namespace 光通信串口调试助手
{
    partial class Frm_Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Help));
            this.rtb_Help = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // rtb_Help
            // 
            this.rtb_Help.BackColor = System.Drawing.Color.White;
            this.rtb_Help.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_Help.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rtb_Help.Location = new System.Drawing.Point(23, 63);
            this.rtb_Help.Name = "rtb_Help";
            this.rtb_Help.ReadOnly = true;
            this.rtb_Help.Size = new System.Drawing.Size(304, 327);
            this.rtb_Help.TabIndex = 0;
            this.rtb_Help.Text = "";
            // 
            // Frm_Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 413);
            this.Controls.Add(this.rtb_Help);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(350, 413);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(350, 413);
            this.Movable = false;
            this.Name = "Frm_Help";
            this.ShadowType = DMSkin.MetroFormShadowType.AeroShadow;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "帮助";
            this.Load += new System.EventHandler(this.Frm_Help_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_Help;
    }
}