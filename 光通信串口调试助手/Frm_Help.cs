﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DMSkin;
using System.IO;

namespace 光通信串口调试助手
{
    public partial class Frm_Help : DMSkin.MetroForm
    {
        public Frm_Help()
        {
            InitializeComponent();
        }

        private void Frm_Help_Load(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(@"帮助.rtf"))
                    rtb_Help.LoadFile(@"帮助.rtf");
                else
                {
                    DMSkin.MetroMessageBox.Show(this.Owner, "帮助文件不存在！", "提示");
                    rtb_Help.Text = "帮助文件不存在！";
                    rtb_Help.SelectAll();
                    rtb_Help.SelectionAlignment = HorizontalAlignment.Center;
                    rtb_Help.SelectionLength = 0;
                }
            }
            catch(Exception err)
            {
                this.Owner.Activate();
                DMSkin.MetroMessageBox.Show(this.Owner, err.Message, "提示");
                rtb_Help.Text = err.Message;
                rtb_Help.SelectAll();
                rtb_Help.SelectionAlignment = HorizontalAlignment.Center;
                rtb_Help.SelectionLength = 0;
            }
        }
    }
}
