﻿namespace 光通信串口调试助手
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.dtbx_Receive = new DMSkin.Controls.DMTextBox();
            this.btn_Send = new DMSkin.Controls.DMButton();
            this.dtbx_Send = new DMSkin.Controls.DMTextBox();
            this.skinLine1 = new CCWin.SkinControl.SkinLine();
            this.skinLine2 = new CCWin.SkinControl.SkinLine();
            this.接收区 = new DMSkin.Metro.Controls.MetroLabel();
            this.skinLine3 = new CCWin.SkinControl.SkinLine();
            this.metroLabel1 = new DMSkin.Metro.Controls.MetroLabel();
            this.dbtn_Clear = new DMSkin.Controls.DMButton();
            this.dbtn_Clear_RCV = new DMSkin.Controls.DMButton();
            this.mcb_Show_Time = new DMSkin.Metro.Controls.MetroCheckBox();
            this.skinLine4 = new CCWin.SkinControl.SkinLine();
            this.metroLabel2 = new DMSkin.Metro.Controls.MetroLabel();
            this.lab_Tip = new DMSkin.Metro.Controls.MetroLabel();
            this.lab_SysTmr = new DMSkin.Metro.Controls.MetroLabel();
            this.tmr_Sys = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.llab_SWorld = new System.Windows.Forms.LinkLabel();
            this.ndp_Times = new System.Windows.Forms.NumericUpDown();
            this.mcb_Show_Now = new DMSkin.Metro.Controls.MetroCheckBox();
            this.mcb_Send_OnTime = new DMSkin.Metro.Controls.MetroCheckBox();
            this.lab_Sta = new DMSkin.Metro.Controls.MetroLabel();
            this.lab_Model = new DMSkin.Metro.Controls.MetroLabel();
            this.metroLabel8 = new DMSkin.Metro.Controls.MetroLabel();
            this.metroLabel7 = new DMSkin.Metro.Controls.MetroLabel();
            this.mcb_Send_NewLIne = new DMSkin.Metro.Controls.MetroCheckBox();
            this.mcb_Send_Hex = new DMSkin.Metro.Controls.MetroCheckBox();
            this.mcb_Color = new DMSkin.Metro.Controls.MetroCheckBox();
            this.mcb_Show_Hex = new DMSkin.Metro.Controls.MetroCheckBox();
            this.metroLabel4 = new DMSkin.Metro.Controls.MetroLabel();
            this.skinLine6 = new CCWin.SkinControl.SkinLine();
            this.btn_fix = new DMSkin.Controls.DMButton();
            this.btn_Help = new DMSkin.Controls.DMButton();
            this.btn_Hand = new DMSkin.Controls.DMButton();
            this.btn_Send_String = new DMSkin.Controls.DMButton();
            this.btn_Send_Voice = new DMSkin.Controls.DMButton();
            this.skinLine5 = new CCWin.SkinControl.SkinLine();
            this.metroLabel3 = new DMSkin.Metro.Controls.MetroLabel();
            this.lab_RCV = new DMSkin.Metro.Controls.MetroLabel();
            this.mlab_RCV_Count = new DMSkin.Metro.Controls.MetroLabel();
            this.metroLabel5 = new DMSkin.Metro.Controls.MetroLabel();
            this.metroLabel6 = new DMSkin.Metro.Controls.MetroLabel();
            this.mlab_Sed_Count = new DMSkin.Metro.Controls.MetroLabel();
            this.mtt_Main = new DMSkin.Metro.Components.MetroToolTip();
            this.tmr_Send_OnTime = new System.Windows.Forms.Timer(this.components);
            this.serial_Con1 = new MySerial.Serial_Con();
            this.tmr_Fresh_Con = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ndp_Times)).BeginInit();
            this.SuspendLayout();
            // 
            // dtbx_Receive
            // 
            this.dtbx_Receive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtbx_Receive.BackColor = System.Drawing.Color.White;
            this.dtbx_Receive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtbx_Receive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.dtbx_Receive.Location = new System.Drawing.Point(16, 140);
            this.dtbx_Receive.Multiline = true;
            this.dtbx_Receive.Name = "dtbx_Receive";
            this.dtbx_Receive.ReadOnly = true;
            this.dtbx_Receive.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dtbx_Receive.Size = new System.Drawing.Size(239, 153);
            this.dtbx_Receive.TabIndex = 2;
            this.dtbx_Receive.WaterText = "";
            // 
            // btn_Send
            // 
            this.btn_Send.BackColor = System.Drawing.Color.Transparent;
            this.btn_Send.DM_DisabledColor = System.Drawing.Color.Empty;
            this.btn_Send.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.btn_Send.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btn_Send.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Send.DM_Radius = 1;
            this.btn_Send.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Send.ForeColor = System.Drawing.Color.White;
            this.btn_Send.Image = null;
            this.btn_Send.Location = new System.Drawing.Point(450, 299);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(59, 30);
            this.btn_Send.TabIndex = 4;
            this.btn_Send.Text = "发送";
            this.mtt_Main.SetToolTip(this.btn_Send, "发送数据");
            this.btn_Send.UseVisualStyleBackColor = false;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // dtbx_Send
            // 
            this.dtbx_Send.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtbx_Send.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.dtbx_Send.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtbx_Send.Location = new System.Drawing.Point(263, 140);
            this.dtbx_Send.Multiline = true;
            this.dtbx_Send.Name = "dtbx_Send";
            this.dtbx_Send.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dtbx_Send.Size = new System.Drawing.Size(246, 127);
            this.dtbx_Send.TabIndex = 1;
            this.dtbx_Send.WaterText = "请输入发送信息";
            this.dtbx_Send.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtbx_Send_KeyPress);
            // 
            // skinLine1
            // 
            this.skinLine1.BackColor = System.Drawing.Color.Transparent;
            this.skinLine1.LineColor = System.Drawing.Color.Gray;
            this.skinLine1.LineHeight = 2;
            this.skinLine1.Location = new System.Drawing.Point(22, 52);
            this.skinLine1.Name = "skinLine1";
            this.skinLine1.Size = new System.Drawing.Size(494, 12);
            this.skinLine1.TabIndex = 5;
            this.skinLine1.Text = "skinLine1";
            // 
            // skinLine2
            // 
            this.skinLine2.BackColor = System.Drawing.Color.Transparent;
            this.skinLine2.LineColor = System.Drawing.Color.Gray;
            this.skinLine2.LineHeight = 1;
            this.skinLine2.Location = new System.Drawing.Point(15, 135);
            this.skinLine2.Name = "skinLine2";
            this.skinLine2.Size = new System.Drawing.Size(241, 10);
            this.skinLine2.TabIndex = 5;
            this.skinLine2.Text = "skinLine1";
            // 
            // 接收区
            // 
            this.接收区.AutoSize = true;
            this.接收区.Location = new System.Drawing.Point(15, 112);
            this.接收区.Name = "接收区";
            this.接收区.Size = new System.Drawing.Size(51, 20);
            this.接收区.TabIndex = 6;
            this.接收区.Text = "接收区";
            // 
            // skinLine3
            // 
            this.skinLine3.BackColor = System.Drawing.Color.Transparent;
            this.skinLine3.LineColor = System.Drawing.Color.Gray;
            this.skinLine3.LineHeight = 1;
            this.skinLine3.Location = new System.Drawing.Point(261, 135);
            this.skinLine3.Name = "skinLine3";
            this.skinLine3.Size = new System.Drawing.Size(248, 10);
            this.skinLine3.TabIndex = 5;
            this.skinLine3.Text = "skinLine1";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(261, 112);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(51, 20);
            this.metroLabel1.TabIndex = 6;
            this.metroLabel1.Text = "发送区";
            // 
            // dbtn_Clear
            // 
            this.dbtn_Clear.BackColor = System.Drawing.Color.Transparent;
            this.dbtn_Clear.DM_DisabledColor = System.Drawing.Color.Empty;
            this.dbtn_Clear.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.dbtn_Clear.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.dbtn_Clear.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.dbtn_Clear.DM_Radius = 1;
            this.dbtn_Clear.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dbtn_Clear.ForeColor = System.Drawing.Color.White;
            this.dbtn_Clear.Image = null;
            this.dbtn_Clear.Location = new System.Drawing.Point(384, 299);
            this.dbtn_Clear.Name = "dbtn_Clear";
            this.dbtn_Clear.Size = new System.Drawing.Size(60, 30);
            this.dbtn_Clear.TabIndex = 4;
            this.dbtn_Clear.Text = "清空";
            this.mtt_Main.SetToolTip(this.dbtn_Clear, "清空发送区数据");
            this.dbtn_Clear.UseVisualStyleBackColor = false;
            this.dbtn_Clear.Click += new System.EventHandler(this.dbtn_Clear_Click);
            // 
            // dbtn_Clear_RCV
            // 
            this.dbtn_Clear_RCV.BackColor = System.Drawing.Color.Transparent;
            this.dbtn_Clear_RCV.DM_DisabledColor = System.Drawing.Color.Empty;
            this.dbtn_Clear_RCV.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.dbtn_Clear_RCV.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.dbtn_Clear_RCV.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.dbtn_Clear_RCV.DM_Radius = 1;
            this.dbtn_Clear_RCV.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dbtn_Clear_RCV.ForeColor = System.Drawing.Color.White;
            this.dbtn_Clear_RCV.Image = null;
            this.dbtn_Clear_RCV.Location = new System.Drawing.Point(195, 297);
            this.dbtn_Clear_RCV.Name = "dbtn_Clear_RCV";
            this.dbtn_Clear_RCV.Size = new System.Drawing.Size(60, 30);
            this.dbtn_Clear_RCV.TabIndex = 4;
            this.dbtn_Clear_RCV.Text = "清空";
            this.mtt_Main.SetToolTip(this.dbtn_Clear_RCV, "点击清空接收区数据");
            this.dbtn_Clear_RCV.UseVisualStyleBackColor = false;
            this.dbtn_Clear_RCV.Click += new System.EventHandler(this.dbtn_Clear_RCV_Click);
            // 
            // mcb_Show_Time
            // 
            this.mcb_Show_Time.AutoSize = true;
            this.mcb_Show_Time.Checked = true;
            this.mcb_Show_Time.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcb_Show_Time.DM_UseSelectable = true;
            this.mcb_Show_Time.Location = new System.Drawing.Point(15, 295);
            this.mcb_Show_Time.Name = "mcb_Show_Time";
            this.mcb_Show_Time.Size = new System.Drawing.Size(60, 17);
            this.mcb_Show_Time.TabIndex = 7;
            this.mcb_Show_Time.Text = "时间戳";
            this.mtt_Main.SetToolTip(this.mcb_Show_Time, "显示接收时间");
            // 
            // skinLine4
            // 
            this.skinLine4.BackColor = System.Drawing.Color.Transparent;
            this.skinLine4.LineColor = System.Drawing.Color.Gray;
            this.skinLine4.LineHeight = 1;
            this.skinLine4.Location = new System.Drawing.Point(15, 26);
            this.skinLine4.Name = "skinLine4";
            this.skinLine4.Size = new System.Drawing.Size(240, 10);
            this.skinLine4.TabIndex = 5;
            this.skinLine4.Text = "skinLine1";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(15, 3);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 20);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "配置串口";
            // 
            // lab_Tip
            // 
            this.lab_Tip.AutoSize = true;
            this.lab_Tip.Location = new System.Drawing.Point(22, 387);
            this.lab_Tip.Name = "lab_Tip";
            this.lab_Tip.Size = new System.Drawing.Size(51, 20);
            this.lab_Tip.TabIndex = 8;
            this.lab_Tip.Text = "就绪！";
            // 
            // lab_SysTmr
            // 
            this.lab_SysTmr.AutoSize = true;
            this.lab_SysTmr.Location = new System.Drawing.Point(362, 29);
            this.lab_SysTmr.Name = "lab_SysTmr";
            this.lab_SysTmr.Size = new System.Drawing.Size(154, 20);
            this.lab_SysTmr.TabIndex = 8;
            this.lab_SysTmr.Text = "0000年00月00日 12:00";
            // 
            // tmr_Sys
            // 
            this.tmr_Sys.Enabled = true;
            this.tmr_Sys.Interval = 1000;
            this.tmr_Sys.Tick += new System.EventHandler(this.tmr_Sys_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.llab_SWorld);
            this.panel1.Controls.Add(this.ndp_Times);
            this.panel1.Controls.Add(this.dbtn_Clear);
            this.panel1.Controls.Add(this.mcb_Show_Now);
            this.panel1.Controls.Add(this.mcb_Send_OnTime);
            this.panel1.Controls.Add(this.lab_Sta);
            this.panel1.Controls.Add(this.lab_Model);
            this.panel1.Controls.Add(this.metroLabel8);
            this.panel1.Controls.Add(this.metroLabel7);
            this.panel1.Controls.Add(this.mcb_Send_NewLIne);
            this.panel1.Controls.Add(this.mcb_Send_Hex);
            this.panel1.Controls.Add(this.mcb_Color);
            this.panel1.Controls.Add(this.mcb_Show_Hex);
            this.panel1.Controls.Add(this.mcb_Show_Time);
            this.panel1.Controls.Add(this.dtbx_Send);
            this.panel1.Controls.Add(this.dtbx_Receive);
            this.panel1.Controls.Add(this.metroLabel1);
            this.panel1.Controls.Add(this.metroLabel4);
            this.panel1.Controls.Add(this.metroLabel2);
            this.panel1.Controls.Add(this.接收区);
            this.panel1.Controls.Add(this.skinLine6);
            this.panel1.Controls.Add(this.skinLine4);
            this.panel1.Controls.Add(this.skinLine3);
            this.panel1.Controls.Add(this.skinLine2);
            this.panel1.Controls.Add(this.dbtn_Clear_RCV);
            this.panel1.Controls.Add(this.btn_fix);
            this.panel1.Controls.Add(this.btn_Help);
            this.panel1.Controls.Add(this.btn_Hand);
            this.panel1.Controls.Add(this.btn_Send_String);
            this.panel1.Controls.Add(this.btn_Send_Voice);
            this.panel1.Controls.Add(this.btn_Send);
            this.panel1.Controls.Add(this.serial_Con1);
            this.panel1.Location = new System.Drawing.Point(7, 54);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(523, 332);
            this.panel1.TabIndex = 9;
            // 
            // llab_SWorld
            // 
            this.llab_SWorld.ActiveLinkColor = System.Drawing.Color.DeepSkyBlue;
            this.llab_SWorld.AutoSize = true;
            this.llab_SWorld.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.llab_SWorld.LinkColor = System.Drawing.Color.Black;
            this.llab_SWorld.Location = new System.Drawing.Point(260, 312);
            this.llab_SWorld.Name = "llab_SWorld";
            this.llab_SWorld.Size = new System.Drawing.Size(94, 16);
            this.llab_SWorld.TabIndex = 11;
            this.llab_SWorld.TabStop = true;
            this.llab_SWorld.Text = "Design By SWorld";
            this.mtt_Main.SetToolTip(this.llab_SWorld, "获取帮助");
            this.llab_SWorld.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llab_SWorld_LinkClicked);
            // 
            // ndp_Times
            // 
            this.ndp_Times.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ndp_Times.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ndp_Times.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ndp_Times.Location = new System.Drawing.Point(442, 273);
            this.ndp_Times.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ndp_Times.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.ndp_Times.Name = "ndp_Times";
            this.ndp_Times.Size = new System.Drawing.Size(67, 23);
            this.ndp_Times.TabIndex = 9;
            this.ndp_Times.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mtt_Main.SetToolTip(this.ndp_Times, "定时发送时间");
            this.ndp_Times.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // mcb_Show_Now
            // 
            this.mcb_Show_Now.AutoSize = true;
            this.mcb_Show_Now.Checked = true;
            this.mcb_Show_Now.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcb_Show_Now.DM_UseSelectable = true;
            this.mcb_Show_Now.Location = new System.Drawing.Point(15, 312);
            this.mcb_Show_Now.Name = "mcb_Show_Now";
            this.mcb_Show_Now.Size = new System.Drawing.Size(72, 17);
            this.mcb_Show_Now.TabIndex = 7;
            this.mcb_Show_Now.Text = "实时显示";
            this.mtt_Main.SetToolTip(this.mcb_Show_Now, "始终显示最后一行");
            // 
            // mcb_Send_OnTime
            // 
            this.mcb_Send_OnTime.AutoSize = true;
            this.mcb_Send_OnTime.DM_UseSelectable = true;
            this.mcb_Send_OnTime.Enabled = false;
            this.mcb_Send_OnTime.Location = new System.Drawing.Point(364, 276);
            this.mcb_Send_OnTime.Name = "mcb_Send_OnTime";
            this.mcb_Send_OnTime.Size = new System.Drawing.Size(72, 17);
            this.mcb_Send_OnTime.TabIndex = 7;
            this.mcb_Send_OnTime.Text = "定时发送";
            this.mtt_Main.SetToolTip(this.mcb_Send_OnTime, "定时发送数据");
            this.mcb_Send_OnTime.CheckedChanged += new System.EventHandler(this.mcb_Send_OnTime_CheckedChanged);
            // 
            // lab_Sta
            // 
            this.lab_Sta.AutoSize = true;
            this.lab_Sta.BackColor = System.Drawing.Color.Transparent;
            this.lab_Sta.DM_UseCustomForeColor = true;
            this.lab_Sta.ForeColor = System.Drawing.Color.Red;
            this.lab_Sta.Location = new System.Drawing.Point(333, 41);
            this.lab_Sta.Name = "lab_Sta";
            this.lab_Sta.Size = new System.Drawing.Size(37, 20);
            this.lab_Sta.TabIndex = 8;
            this.lab_Sta.Text = "断开";
            this.mtt_Main.SetToolTip(this.lab_Sta, "双击刷新状态");
            this.lab_Sta.TextChanged += new System.EventHandler(this.lab_Sta_TextChanged);
            this.lab_Sta.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lab_Sta_MouseDoubleClick);
            // 
            // lab_Model
            // 
            this.lab_Model.AutoSize = true;
            this.lab_Model.Location = new System.Drawing.Point(333, 76);
            this.lab_Model.Name = "lab_Model";
            this.lab_Model.Size = new System.Drawing.Size(51, 20);
            this.lab_Model.TabIndex = 8;
            this.lab_Model.Text = "字符串";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(263, 76);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(79, 20);
            this.metroLabel8.TabIndex = 8;
            this.metroLabel8.Text = "发送模式：";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(263, 41);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(79, 20);
            this.metroLabel7.TabIndex = 8;
            this.metroLabel7.Text = "通讯状态：";
            // 
            // mcb_Send_NewLIne
            // 
            this.mcb_Send_NewLIne.AutoSize = true;
            this.mcb_Send_NewLIne.Checked = true;
            this.mcb_Send_NewLIne.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcb_Send_NewLIne.DM_UseSelectable = true;
            this.mcb_Send_NewLIne.Location = new System.Drawing.Point(262, 295);
            this.mcb_Send_NewLIne.Name = "mcb_Send_NewLIne";
            this.mcb_Send_NewLIne.Size = new System.Drawing.Size(72, 17);
            this.mcb_Send_NewLIne.TabIndex = 7;
            this.mcb_Send_NewLIne.Text = "发送新行";
            this.mtt_Main.SetToolTip(this.mcb_Send_NewLIne, "发送数据之后添加\\r\\n");
            // 
            // mcb_Send_Hex
            // 
            this.mcb_Send_Hex.AutoSize = true;
            this.mcb_Send_Hex.DM_UseSelectable = true;
            this.mcb_Send_Hex.Location = new System.Drawing.Point(262, 276);
            this.mcb_Send_Hex.Name = "mcb_Send_Hex";
            this.mcb_Send_Hex.Size = new System.Drawing.Size(96, 17);
            this.mcb_Send_Hex.TabIndex = 7;
            this.mcb_Send_Hex.Text = "十六进制发送";
            this.mtt_Main.SetToolTip(this.mcb_Send_Hex, "十六进制发送数据");
            this.mcb_Send_Hex.CheckedChanged += new System.EventHandler(this.mcb_Send_Hex_CheckedChanged);
            // 
            // mcb_Color
            // 
            this.mcb_Color.AutoSize = true;
            this.mcb_Color.DM_UseSelectable = true;
            this.mcb_Color.Location = new System.Drawing.Point(95, 312);
            this.mcb_Color.Name = "mcb_Color";
            this.mcb_Color.Size = new System.Drawing.Size(72, 17);
            this.mcb_Color.TabIndex = 7;
            this.mcb_Color.Text = "黑底白字";
            this.mtt_Main.SetToolTip(this.mcb_Color, "接收区背景变为黑底白字");
            this.mcb_Color.CheckedChanged += new System.EventHandler(this.mcb_Color_CheckedChanged);
            // 
            // mcb_Show_Hex
            // 
            this.mcb_Show_Hex.AutoSize = true;
            this.mcb_Show_Hex.DM_UseSelectable = true;
            this.mcb_Show_Hex.Location = new System.Drawing.Point(95, 295);
            this.mcb_Show_Hex.Name = "mcb_Show_Hex";
            this.mcb_Show_Hex.Size = new System.Drawing.Size(96, 17);
            this.mcb_Show_Hex.TabIndex = 7;
            this.mcb_Show_Hex.Text = "十六进制显示";
            this.mtt_Main.SetToolTip(this.mcb_Show_Hex, "十六进制显示数据");
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(262, 3);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(65, 20);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "控制面板";
            // 
            // skinLine6
            // 
            this.skinLine6.BackColor = System.Drawing.Color.Transparent;
            this.skinLine6.LineColor = System.Drawing.Color.Gray;
            this.skinLine6.LineHeight = 1;
            this.skinLine6.Location = new System.Drawing.Point(263, 26);
            this.skinLine6.Name = "skinLine6";
            this.skinLine6.Size = new System.Drawing.Size(246, 10);
            this.skinLine6.TabIndex = 5;
            this.skinLine6.Text = "skinLine1";
            // 
            // btn_fix
            // 
            this.btn_fix.BackColor = System.Drawing.Color.White;
            this.btn_fix.BackgroundImage = global::光通信串口调试助手.Properties.Resources.unfix;
            this.btn_fix.DM_DisabledColor = System.Drawing.Color.Transparent;
            this.btn_fix.DM_DownColor = System.Drawing.Color.Transparent;
            this.btn_fix.DM_MoveColor = System.Drawing.Color.Transparent;
            this.btn_fix.DM_NormalColor = System.Drawing.Color.Transparent;
            this.btn_fix.DM_Radius = 1;
            this.btn_fix.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_fix.ForeColor = System.Drawing.Color.White;
            this.btn_fix.Image = null;
            this.btn_fix.Location = new System.Drawing.Point(493, 7);
            this.btn_fix.Name = "btn_fix";
            this.btn_fix.Size = new System.Drawing.Size(16, 16);
            this.btn_fix.TabIndex = 4;
            this.btn_fix.Text = "";
            this.mtt_Main.SetToolTip(this.btn_fix, "置顶");
            this.btn_fix.UseVisualStyleBackColor = false;
            this.btn_fix.Click += new System.EventHandler(this.btn_fix_Click);
            // 
            // btn_Help
            // 
            this.btn_Help.BackColor = System.Drawing.Color.Transparent;
            this.btn_Help.DM_DisabledColor = System.Drawing.Color.Empty;
            this.btn_Help.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.btn_Help.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btn_Help.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Help.DM_Radius = 1;
            this.btn_Help.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Help.ForeColor = System.Drawing.Color.White;
            this.btn_Help.Image = null;
            this.btn_Help.Location = new System.Drawing.Point(450, 37);
            this.btn_Help.Name = "btn_Help";
            this.btn_Help.Size = new System.Drawing.Size(59, 30);
            this.btn_Help.TabIndex = 4;
            this.btn_Help.Text = "帮助";
            this.mtt_Main.SetToolTip(this.btn_Help, "打开帮助文档");
            this.btn_Help.UseVisualStyleBackColor = false;
            this.btn_Help.Click += new System.EventHandler(this.btn_Help_Click);
            // 
            // btn_Hand
            // 
            this.btn_Hand.BackColor = System.Drawing.Color.Transparent;
            this.btn_Hand.DM_DisabledColor = System.Drawing.Color.Empty;
            this.btn_Hand.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.btn_Hand.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btn_Hand.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Hand.DM_Radius = 1;
            this.btn_Hand.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Hand.ForeColor = System.Drawing.Color.White;
            this.btn_Hand.Image = null;
            this.btn_Hand.Location = new System.Drawing.Point(385, 37);
            this.btn_Hand.Name = "btn_Hand";
            this.btn_Hand.Size = new System.Drawing.Size(59, 30);
            this.btn_Hand.TabIndex = 4;
            this.btn_Hand.Text = "握手";
            this.mtt_Main.SetToolTip(this.btn_Hand, "与设备建立连接");
            this.btn_Hand.UseVisualStyleBackColor = false;
            this.btn_Hand.Click += new System.EventHandler(this.btn_Hand_Click);
            // 
            // btn_Send_String
            // 
            this.btn_Send_String.BackColor = System.Drawing.Color.Transparent;
            this.btn_Send_String.DM_DisabledColor = System.Drawing.Color.Empty;
            this.btn_Send_String.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.btn_Send_String.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btn_Send_String.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Send_String.DM_Radius = 1;
            this.btn_Send_String.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Send_String.ForeColor = System.Drawing.Color.White;
            this.btn_Send_String.Image = null;
            this.btn_Send_String.Location = new System.Drawing.Point(385, 71);
            this.btn_Send_String.Name = "btn_Send_String";
            this.btn_Send_String.Size = new System.Drawing.Size(59, 30);
            this.btn_Send_String.TabIndex = 4;
            this.btn_Send_String.Text = "字符串";
            this.mtt_Main.SetToolTip(this.btn_Send_String, "切换至字符串发送模式");
            this.btn_Send_String.UseVisualStyleBackColor = false;
            this.btn_Send_String.Click += new System.EventHandler(this.btn_Send_String_Click);
            // 
            // btn_Send_Voice
            // 
            this.btn_Send_Voice.BackColor = System.Drawing.Color.Transparent;
            this.btn_Send_Voice.DM_DisabledColor = System.Drawing.Color.Empty;
            this.btn_Send_Voice.DM_DownColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(140)))), ((int)(((byte)(188)))));
            this.btn_Send_Voice.DM_MoveColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(195)))), ((int)(((byte)(245)))));
            this.btn_Send_Voice.DM_NormalColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Send_Voice.DM_Radius = 1;
            this.btn_Send_Voice.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Send_Voice.ForeColor = System.Drawing.Color.White;
            this.btn_Send_Voice.Image = null;
            this.btn_Send_Voice.Location = new System.Drawing.Point(450, 71);
            this.btn_Send_Voice.Name = "btn_Send_Voice";
            this.btn_Send_Voice.Size = new System.Drawing.Size(59, 30);
            this.btn_Send_Voice.TabIndex = 4;
            this.btn_Send_Voice.Text = "音频";
            this.mtt_Main.SetToolTip(this.btn_Send_Voice, "切换至音频发送模式");
            this.btn_Send_Voice.UseVisualStyleBackColor = false;
            this.btn_Send_Voice.Click += new System.EventHandler(this.btn_Send_Voice_Click);
            // 
            // skinLine5
            // 
            this.skinLine5.BackColor = System.Drawing.Color.Transparent;
            this.skinLine5.LineColor = System.Drawing.Color.Gray;
            this.skinLine5.LineHeight = 1;
            this.skinLine5.Location = new System.Drawing.Point(22, 385);
            this.skinLine5.Name = "skinLine5";
            this.skinLine5.Size = new System.Drawing.Size(494, 10);
            this.skinLine5.TabIndex = 5;
            this.skinLine5.Text = "skinLine1";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(203, 387);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(51, 20);
            this.metroLabel3.TabIndex = 8;
            this.metroLabel3.Text = "接收：";
            // 
            // lab_RCV
            // 
            this.lab_RCV.AutoSize = true;
            this.lab_RCV.Location = new System.Drawing.Point(302, 387);
            this.lab_RCV.Name = "lab_RCV";
            this.lab_RCV.Size = new System.Drawing.Size(37, 20);
            this.lab_RCV.TabIndex = 8;
            this.lab_RCV.Text = "字节";
            // 
            // mlab_RCV_Count
            // 
            this.mlab_RCV_Count.DM_UseCustomForeColor = true;
            this.mlab_RCV_Count.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.mlab_RCV_Count.Location = new System.Drawing.Point(245, 388);
            this.mlab_RCV_Count.Name = "mlab_RCV_Count";
            this.mlab_RCV_Count.Size = new System.Drawing.Size(61, 20);
            this.mlab_RCV_Count.TabIndex = 8;
            this.mlab_RCV_Count.Text = "0";
            this.mlab_RCV_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtt_Main.SetToolTip(this.mlab_RCV_Count, "双击清零");
            this.mlab_RCV_Count.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.mlab_RCV_Count_MouseDoubleClick);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(380, 387);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(51, 20);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "发送：";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(479, 387);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(37, 20);
            this.metroLabel6.TabIndex = 8;
            this.metroLabel6.Text = "字节";
            // 
            // mlab_Sed_Count
            // 
            this.mlab_Sed_Count.BackColor = System.Drawing.Color.White;
            this.mlab_Sed_Count.DM_UseCustomForeColor = true;
            this.mlab_Sed_Count.ForeColor = System.Drawing.Color.LimeGreen;
            this.mlab_Sed_Count.Location = new System.Drawing.Point(422, 388);
            this.mlab_Sed_Count.Name = "mlab_Sed_Count";
            this.mlab_Sed_Count.Size = new System.Drawing.Size(61, 20);
            this.mlab_Sed_Count.TabIndex = 8;
            this.mlab_Sed_Count.Text = "0";
            this.mlab_Sed_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtt_Main.SetToolTip(this.mlab_Sed_Count, "双击清零");
            this.mlab_Sed_Count.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.mlab_Sed_Count_MouseDoubleClick);
            // 
            // mtt_Main
            // 
            this.mtt_Main.AutoPopDelay = 3000;
            this.mtt_Main.InitialDelay = 500;
            this.mtt_Main.ReshowDelay = 100;
            this.mtt_Main.Style = DMSkin.Metro.MetroColorStyle.Blue;
            this.mtt_Main.StyleManager = null;
            this.mtt_Main.Theme = DMSkin.Metro.MetroThemeStyle.Light;
            // 
            // tmr_Send_OnTime
            // 
            this.tmr_Send_OnTime.Tick += new System.EventHandler(this.tmr_Send_OnTime_Tick);
            // 
            // serial_Con1
            // 
            this.serial_Con1.BackColor = System.Drawing.Color.White;
            this.serial_Con1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.serial_Con1.Location = new System.Drawing.Point(15, 33);
            this.serial_Con1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.serial_Con1.MaximumSize = new System.Drawing.Size(240, 72);
            this.serial_Con1.MinimumSize = new System.Drawing.Size(240, 72);
            this.serial_Con1.Name = "serial_Con1";
            this.serial_Con1.Size = new System.Drawing.Size(240, 72);
            this.serial_Con1.TabIndex = 0;
            this.serial_Con1.Serial_Received += new System.EventHandler(this.serial_Con1_Serial_Received);
            this.serial_Con1.Serial_StataChange += new System.EventHandler(this.serial_Con1_Serial_StataChange);
            // 
            // tmr_Fresh_Con
            // 
            this.tmr_Fresh_Con.Interval = 1000;
            this.tmr_Fresh_Con.Tick += new System.EventHandler(this.tmr_Fresh_Con_Tick);
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 413);
            this.Controls.Add(this.mlab_Sed_Count);
            this.Controls.Add(this.mlab_RCV_Count);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.lab_RCV);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.lab_Tip);
            this.Controls.Add(this.skinLine5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lab_SysTmr);
            this.Controls.Add(this.skinLine1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(539, 413);
            this.MinimumSize = new System.Drawing.Size(539, 413);
            this.Name = "Frm_Main";
            this.Opacity = 0D;
            this.ShadowType = DMSkin.MetroFormShadowType.AeroShadow;
            this.Text = "光通信串口调试助手";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ndp_Times)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MySerial.Serial_Con serial_Con1;
        private DMSkin.Controls.DMTextBox dtbx_Receive;
        private DMSkin.Controls.DMButton btn_Send;
        private DMSkin.Controls.DMTextBox dtbx_Send;
        private CCWin.SkinControl.SkinLine skinLine1;
        private CCWin.SkinControl.SkinLine skinLine2;
        private DMSkin.Metro.Controls.MetroLabel 接收区;
        private CCWin.SkinControl.SkinLine skinLine3;
        private DMSkin.Metro.Controls.MetroLabel metroLabel1;
        private DMSkin.Controls.DMButton dbtn_Clear;
        private DMSkin.Controls.DMButton dbtn_Clear_RCV;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Show_Time;
        private CCWin.SkinControl.SkinLine skinLine4;
        private DMSkin.Metro.Controls.MetroLabel metroLabel2;
        private DMSkin.Metro.Controls.MetroLabel lab_Tip;
        private DMSkin.Metro.Controls.MetroLabel lab_SysTmr;
        private System.Windows.Forms.Timer tmr_Sys;
        private System.Windows.Forms.Panel panel1;
        private CCWin.SkinControl.SkinLine skinLine5;
        private DMSkin.Metro.Controls.MetroLabel metroLabel3;
        private DMSkin.Metro.Controls.MetroLabel lab_RCV;
        private DMSkin.Metro.Controls.MetroLabel mlab_RCV_Count;
        private DMSkin.Metro.Controls.MetroLabel metroLabel5;
        private DMSkin.Metro.Controls.MetroLabel metroLabel6;
        private DMSkin.Metro.Controls.MetroLabel mlab_Sed_Count;
        private DMSkin.Metro.Components.MetroToolTip mtt_Main;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Show_Now;
        private DMSkin.Metro.Controls.MetroLabel metroLabel4;
        private CCWin.SkinControl.SkinLine skinLine6;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Show_Hex;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Send_Hex;
        private System.Windows.Forms.NumericUpDown ndp_Times;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Send_OnTime;
        private System.Windows.Forms.Timer tmr_Send_OnTime;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Send_NewLIne;
        private System.Windows.Forms.LinkLabel llab_SWorld;
        private DMSkin.Metro.Controls.MetroCheckBox mcb_Color;
        private DMSkin.Controls.DMButton btn_fix;
        private DMSkin.Controls.DMButton btn_Send_Voice;
        private DMSkin.Controls.DMButton btn_Help;
        private DMSkin.Metro.Controls.MetroLabel metroLabel8;
        private DMSkin.Metro.Controls.MetroLabel metroLabel7;
        private DMSkin.Metro.Controls.MetroLabel lab_Sta;
        private DMSkin.Metro.Controls.MetroLabel lab_Model;
        private DMSkin.Controls.DMButton btn_Hand;
        private DMSkin.Controls.DMButton btn_Send_String;
        private System.Windows.Forms.Timer tmr_Fresh_Con;
    }
}