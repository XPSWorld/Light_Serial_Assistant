﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 光通信串口调试助手
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //显示加载界面
            Class.Class_Splasher.Show(typeof(Frm_Load));
            Application.DoEvents();
            Application.Run(new Frm_Main());
        }
    }
}
