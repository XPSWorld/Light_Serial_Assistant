﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using 光通信串口调试助手.Class;

namespace 光通信串口调试助手
{
    public partial class Frm_Load : Form, ISplashForm
    {
        public Frm_Load()
        {
            InitializeComponent();
        }
        #region ISplashForm 成员

        void ISplashForm.SetStatusInfo(string NewStatusInfo)
        {
            lab_Tip.Text = NewStatusInfo;
        }

        #endregion

        private void Frm_Load_Load(object sender, EventArgs e)
        {
            Win32.AnimateWindow(this.Handle, 600, Win32.AW_BLEND);
        }
    }

    /// <summary>
    /// 显示效果类
    /// </summary>
    public class Win32
    {
        public const Int32 AW_BLEND = 0x00080000; // 淡入淡出效果
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool AnimateWindow(
        IntPtr hwnd, // handle to window
        int dwTime, // duration of animation
        int dwFlags // animation type
        );
    }
}
