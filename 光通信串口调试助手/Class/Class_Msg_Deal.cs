﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 光通信串口调试助手.Class
{
    /// <summary>
    /// 串口消息处理类 
    /// </summary>
    public class Class_Msg_Deal
    {
        string _str_Rcv ;       //接收字符串
        public Class_Msg_Deal(string  _str)
        {
            _str_Rcv = _str;
        }

        /// <summary>
        /// 解析线程
        /// </summary>
        public void Thread_Parse(object _obj)
        {
            //_str_Rcv = Class.Data_Convert.HexStringToString(_str_Rcv,Encoding.ASCII);           //转换为字符串
            Frm_Main.CallBackDelegate cbd = _obj as Frm_Main.CallBackDelegate;
            switch (_str_Rcv)
            {
                case "OK\r\n":      //接收成功回执
                    cbd(1);
                    break;
                case "CON\r\n":     //握手成功回执
                    cbd(1);
                    break;
                case "OK_MS\r\n":       //切换字符串模式回执
                    cbd(2);
                    break;
                case "OK_MV\r\n":       //切换音频模式回执
                    cbd(3);
                    break;
                case "CONS\r\n":       //连接回执
                    cbd(0);
                    break;
            }
        }
    }
}
