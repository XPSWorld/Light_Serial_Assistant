﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMSkin.Metro.Controls;

namespace 光通信串口调试助手.Class
{
    public class Var_Monitor
    {
        MetroLabel mlab_rcv,mlab_send;

        //需要监控的字段
        private int _Rcv_Count,_Send_Count;

        //属性设置，此处调用了事件触发函数
        public int Rcv_Count
        {
            get { return _Rcv_Count; }
            set
            {
                //如果变量改变则调用事件触发函数
                if (value != _Rcv_Count)
                {
                    _Rcv_Count = value;
                    When_Rcv_Change();
                }
            }
        }

        //属性设置，此处调用了事件触发函数
        public int Send_Count
        {
            get { return _Send_Count; }
            set
            {
                //如果变量改变则调用事件触发函数
                if (value != _Send_Count)
                {
                    _Send_Count = value;
                    When_Send_Change();
                }
            }
        }

        //定义的委托
        public delegate void rcv_Changed(object sender, EventArgs e);
        //与委托相关联的事件
        public event rcv_Changed On_Rcv_Changed;

        //定义的委托
        public delegate void send_Changed(object sender, EventArgs e);
        //与委托相关联的事件
        public event send_Changed On_Send_Changed;

        //构造函数初始化初值并绑定一个事件处理函数
        public Var_Monitor(MetroLabel mlab_rcv,MetroLabel mlab_send)
        {
            _Rcv_Count = 0;
            _Send_Count = 0;
            On_Rcv_Changed += new rcv_Changed(after_Rcv_Changed);
            On_Send_Changed += new send_Changed(after_Send_Changed);
            this.mlab_rcv = mlab_rcv;
            this.mlab_send = mlab_send;
        }

        //事件处理函数，在这里添加变量改变之后的操作
        private void after_Rcv_Changed(object sender, EventArgs e)
        {
            mlab_rcv.Text = _Rcv_Count.ToString();
        }

        //事件处理函数，在这里添加变量改变之后的操作
        private void after_Send_Changed(object sender, EventArgs e)
        {
            mlab_send.Text = _Send_Count.ToString();
        }

        //事件触发函数
        private void When_Rcv_Change()
        {
            if (On_Rcv_Changed != null)
            {
                On_Rcv_Changed(this, null);
            }
        }

        //事件触发函数
        private void When_Send_Change()
        {
            if (On_Send_Changed != null)
            {
                On_Send_Changed(this, null);
            }
        }
    }
}
