﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;


namespace 光通信串口调试助手.Class
{
    public  class Class_Sys_Info
    {
        private bool _ShowTime=true;
        private bool _Fresh=true;
        private bool _ShowHex = false;
        private bool _BlackWhite = false;
        private bool _SendHex = false;
        private bool _SendNewLine = false;
        private int _STime = 1000;
        private string _str_HandShake = "CON_HS";
        private string _str_ModelStr = "CMD_MS";
        private string _str_ModelVoice = "CMD_MV";
        private string _str_CheckCon = "CON_OK";
        private string _str_Path = ""; 
        
        /// <summary>
        /// 构造函数
        /// </summary>
        public Class_Sys_Info()
        {
            _str_Path = Path.Combine(Environment.CurrentDirectory,"SysConfig.ini");
            if (!File.Exists(_str_Path))     //配置文件不存在，创建配置文件
            {
                using (FileStream _fs = File.Create(_str_Path)) { };
                Save(false);
            }
            else
            {
                _ShowTime = bool.Parse(IniHelper.Ini_Read("SysConfig", "ShowTime", _str_Path));
                _Fresh = bool.Parse(IniHelper.Ini_Read("SysConfig", "Fresh", _str_Path));
                _ShowHex = bool.Parse(IniHelper.Ini_Read("SysConfig", "ShowHex", _str_Path));
                _BlackWhite = bool.Parse(IniHelper.Ini_Read("SysConfig", "BlackWhite", _str_Path));
                _SendHex = bool.Parse(IniHelper.Ini_Read("SysConfig", "SendHex", _str_Path));
                _SendNewLine = bool.Parse(IniHelper.Ini_Read("SysConfig", "SendNewLine", _str_Path));
                _STime = int.Parse(IniHelper.Ini_Read("SysConfig", "STime", _str_Path));
                _str_HandShake = IniHelper.Ini_Read("Command", "HandShake", _str_Path);
                _str_ModelStr = IniHelper.Ini_Read("Command", "ModelStr", _str_Path);
                _str_ModelVoice = IniHelper.Ini_Read("Command", "ModelVoice", _str_Path);
                _str_CheckCon= IniHelper.Ini_Read("Command", "CheckCon", _str_Path);
            }
        }

        /// <summary>
        /// 保存系统配置
        /// </summary>
        /// <param name="_b">文件是否存在或者是否需要保存指令</param>
        public void Save(bool _b = true)
        {
            IniHelper.Ini_Write("SysConfig", "ShowTime", _ShowTime.ToString(), _str_Path);
            IniHelper.Ini_Write("SysConfig", "Fresh", _Fresh.ToString(), _str_Path);
            IniHelper.Ini_Write("SysConfig", "ShowHex", _ShowHex.ToString(), _str_Path);
            IniHelper.Ini_Write("SysConfig", "BlackWhite", _BlackWhite.ToString(), _str_Path);
            IniHelper.Ini_Write("SysConfig", "SendHex", _SendHex.ToString(), _str_Path);
            IniHelper.Ini_Write("SysConfig", "SendNewLine", _SendNewLine.ToString(), _str_Path);
            IniHelper.Ini_Write("SysConfig", "STime", _STime.ToString(), _str_Path);
            if (!_b)
            {
                IniHelper.Ini_Write("Command", "HandShake", _str_HandShake, _str_Path);
                IniHelper.Ini_Write("Command", "ModelStr", _str_ModelStr, _str_Path);
                IniHelper.Ini_Write("Command", "ModelVoice", _str_ModelVoice, _str_Path);
                IniHelper.Ini_Write("Command", "CheckCon", _str_CheckCon, _str_Path);
            }
        }

        /// <summary>
        /// 实时显示
        /// </summary>
        public bool ShowTime { get => _ShowTime; set => _ShowTime = value; }
        /// <summary>
        /// 实时刷新
        /// </summary>
        public bool Fresh { get => _Fresh; set => _Fresh = value; }
        /// <summary>
        /// 十六进制显示
        /// </summary>
        public bool ShowHex { get => _ShowHex; set => _ShowHex = value; }
        /// <summary>
        /// 黑底白字
        /// </summary>
        public bool BlackWhite { get => _BlackWhite; set => _BlackWhite = value; }
        /// <summary>
        /// 十六进制发送
        /// </summary>
        public bool SendHex { get => _SendHex; set => _SendHex = value; }
        /// <summary>
        /// 发送新行
        /// </summary>
        public bool SendNewLine { get => _SendNewLine; set => _SendNewLine = value; }
        /// <summary>
        /// 握手指令
        /// </summary>
        public string Str_HandShake { get => _str_HandShake; set => _str_HandShake = value; }
        /// <summary>
        /// 切换字符串模式
        /// </summary>
        public string Str_ModelStr { get => _str_ModelStr; set => _str_ModelStr = value; }
        /// <summary>
        /// 切换至音频模式
        /// </summary>
        public string Str_ModelVoice { get => _str_ModelVoice; set => _str_ModelVoice = value; }
        /// <summary>
        /// 设置自动发送时间
        /// </summary>
        public int STime { get => _STime; set => _STime = value; }
        /// <summary>
        /// 判断连接指令 
        /// </summary>
        public string Str_CheckCon { get => _str_CheckCon; set => _str_CheckCon = value; }
    }
}
