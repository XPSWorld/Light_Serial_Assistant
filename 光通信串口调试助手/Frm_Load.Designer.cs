﻿namespace 光通信串口调试助手
{
    partial class Frm_Load
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Load));
            this.lab_Tip = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lab_Tip
            // 
            this.lab_Tip.AutoSize = true;
            this.lab_Tip.BackColor = System.Drawing.Color.Transparent;
            this.lab_Tip.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lab_Tip.Location = new System.Drawing.Point(11, 169);
            this.lab_Tip.Name = "lab_Tip";
            this.lab_Tip.Size = new System.Drawing.Size(41, 17);
            this.lab_Tip.TabIndex = 0;
            this.lab_Tip.Text = "就绪...";
            // 
            // Frm_Load
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(350, 216);
            this.Controls.Add(this.lab_Tip);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(350, 216);
            this.MinimumSize = new System.Drawing.Size(350, 216);
            this.Name = "Frm_Load";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Load";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Frm_Load_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lab_Tip;
    }
}